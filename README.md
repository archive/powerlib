# PowerLib
This is a Blender 2.8+ port of the Powerlib Addon developed for Agent 327: Operation Barbershop.
The Addon was created by Inês Almeida, Francesco Siddi, Olivier Amrein and Dalai Felinto based on work done by Bassam Kurdali for the Tube Project.

## Installation
Copy or symlink the folder to your Blender user Addon folder.

## How to use
The Powerlib tab is located in the 3D View properties panel. Create an empty assets.json file in your project 
repository (this way you can also version control it).

Select your json file in the powelib settings.